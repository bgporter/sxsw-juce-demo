/*
  ==============================================================================

    controlView.cpp
    Author:  Brett Porter

  ==============================================================================
*/

#include "../../JuceLibraryCode/JuceHeader.h"
#include "controlView.h"

#include "addTimerComponent.h"

#include "../model/timerModel.h"


//==============================================================================
ControlView::ControlView(TimerModel* model)
:  fModel(model)
,  fAddTimer("add timer", DrawableButton::ImageStretched)
,  fClearTimers("clear timers", DrawableButton::ImageStretched)
{
   // NOTE that you need to use the initialization syntax here, not 
   // std::unique_ptr<Drawable< = (...)
   std::unique_ptr<Drawable> add{Drawable::createFromImageData(
      BinaryData::outlineadd_circle_outline24px_svg,
      BinaryData::outlineadd_circle_outline24px_svgSize)};
      
   // setImages() will create internal copies of the image(s)
   fAddTimer.setImages(add.get());
   fAddTimer.addListener(this);
   this->addAndMakeVisible(fAddTimer);
   
   std::unique_ptr<Drawable> clear{Drawable::createFromImageData(
      BinaryData::outlineclear24px_svg,
      BinaryData::outlineclear24px_svgSize)};
      
   fClearTimers.setImages(clear.get());
   fClearTimers.addListener(this);
   this->addAndMakeVisible(fClearTimers);
   
   fModel->addChangeListener(this);

}

ControlView::~ControlView()
{
   fModel->removeChangeListener(this);
}

void ControlView::paint (Graphics& g)
{
    g.fillAll (Colours::lightgrey);

    g.setColour (Colours::grey);
    g.drawRect (getLocalBounds(), 1);   // draw an outline around the component
}

void ControlView::resized()
{
   auto bounds = this->getLocalBounds();
   auto clearBounds = bounds.removeFromLeft(75);
   fClearTimers.setBounds(clearBounds);
   
   auto addBounds = bounds.removeFromRight(75);
   fAddTimer.setBounds(addBounds);

}


void ControlView::buttonClicked(Button* b)
{
   if (b == &fAddTimer)
   {
      std::unique_ptr<Component> dialog{new AddTimerComponent(fModel)};
      
      int result = DialogWindow::showModalDialog(
         TRANS("Add New Timer"),
         dialog.get(), 
         nullptr, // center over entire app.  
         Colours::lightgrey, 
         true,    // escape cancels 
         false,   // is resizable
         false    // has resizer 
      );
      
      DBG("dialog result = " << result);
      
      
   }
   else  if (b == &fClearTimers)
   {
      int cleared = fModel->ClearCompletedTimers();
      DBG("Cleared " << cleared << " timer(s)");
   }
   
}


void ControlView::changeListenerCallback(ChangeBroadcaster* cb)
{
   if (cb == fModel)
   {
      // enable/disable the clear button. 
      fClearTimers.setEnabled(fModel->GetCompleteTimerCount() > 0);
   }
}
