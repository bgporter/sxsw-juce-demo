/*
  ==============================================================================

    controlView.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"

class TimerModel;

class ControlView    : public Component
                     , public Button::Listener
                     , public ChangeListener
{
public:
   ControlView(TimerModel* model);
   ~ControlView();

   void paint (Graphics&) override;
   void resized() override;
    
   void buttonClicked(Button* b) override;
   
   /**
    * We listen to the model for timers to complete -- when this happens, we 
    * enable/disable the clear button 
    * @param cb source of the change. 
    */
   void changeListenerCallback(ChangeBroadcaster* cb) override;

private:
   JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ControlView)
    
   TimerModel* fModel;
    
    
   /// Click this to add a new timer to the app. 
   DrawableButton fAddTimer;
    
   /// Click this to delete any timers that are finished. 
   DrawableButton fClearTimers;
};
