/*
  ==============================================================================

    addTimerComponent.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"


class TimerModel;

class AddTimerComponent : public Component
                        , public Button::Listener
{
public:
   AddTimerComponent(TimerModel* model);
   ~AddTimerComponent();

   void paint (Graphics&) override;
   void resized() override;
    
   void buttonClicked(Button* b) override;

private: 
   
   int ParseDuration(const String& durationString);


private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AddTimerComponent)
    
    TimerModel* fModel;
    
    std::unique_ptr<TextEditor> fTimerName;
    std::unique_ptr<Label> fNameLabel;
    
    std::unique_ptr<TextEditor> fTimerDuration;
    std::unique_ptr<Label> fDurationLabel;
    
    std::unique_ptr<TextButton> fOkButton;
    std::unique_ptr<TextButton> fCancelButton;
    
    
    
    
};
