/*
  ==============================================================================

    timerView.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"


class TimerItem;

class TimerView   : public Component
                  , public ChangeListener
                  , public Label::Listener
{
public:
   TimerView(TimerItem* timer);
   ~TimerView();

   void paint (Graphics&) override;
   void resized() override;
   
   void changeListenerCallback(ChangeBroadcaster* source) override;
   
   void labelTextChanged(Label* label) override ;
    

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimerView)
    
    /// The timer object that we will represent on screen. 
    TimerItem* fTimer;
    
    std::unique_ptr<Label> fTimerName;
    std::unique_ptr<Label> fTimeRemaining; 
};
