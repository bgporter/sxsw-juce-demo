/*
  ==============================================================================

    timerViewport.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"

class TimerViewport  : public Viewport 
{
public:
   TimerViewport()=default;
   ~TimerViewport()=default;
   
   
   /**
    * We only need to create a custom class here to override this 
    * `resized()` behavior here -- the Viewport on its own is exactly that, a 
    * view onto something larger. In this case, we want to make sure that the 
    * component holding the list of timers is also resized to match the width 
    * of the viewport. 
    */
   void resized() override 
   {
      auto bounds = this->getLocalBounds();
      auto content = this->getViewedComponent();
      if (nullptr != content)
      {
         // tell our content how wide it needs to make itself. 
         content->setSize(bounds.getWidth(), content->getHeight());
      }
   }
   
   
   
};
