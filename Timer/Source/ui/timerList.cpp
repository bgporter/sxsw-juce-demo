/*
  ==============================================================================

    timerList.cpp
    Author:  Brett Porter

  ==============================================================================
*/

#include "../../JuceLibraryCode/JuceHeader.h"
#include "timerList.h"
#include "timerView.h"
#include "../model/timerModel.h"

//==============================================================================
TimerList::TimerList(TimerModel& model)
:  fModel(model)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    
    // subscribe to any changes from the model so we can update ourselves. 
    fModel.addChangeListener(this);
    this->Rebuild();

}

TimerList::~TimerList()
{
   // unsubscribe from the model's change updates. 
   fModel.removeChangeListener(this);
}

void TimerList::paint (Graphics& g)
{
   // just paint the background
    g.fillAll (Colours::red);
}

void TimerList::resized()
{
   int yPos = 0;
   auto bounds = this->getLocalBounds();
   
   DBG("TimerList::resized() w = " << bounds.getWidth() << " h: " << bounds.getHeight());
   // stack all of the timer views, with timer[9] at the top and growing 
   // downward. 
   for (auto view: fTimers)
   {
      int height = view->getHeight();
      view->setBounds(0, yPos, bounds.getWidth(), height);
      
      yPos += height;
   }

}

void TimerList::changeListenerCallback(ChangeBroadcaster* source)
{
   // the only reason we'll get notified of a change is because a timer 
   // was added or deleted; rebuild the view list. 
   if (fTimers.size() != fModel.Size())
   {
      this->Rebuild();
   }
}


void TimerList::Rebuild()
{
   fTimers.clear();
   
   int timerCount = fModel.Size();
   int newHeight = 0;
   for (int i = 0; i < timerCount; ++i)
   {
      // create a new timer view component, passing it the model object 
      // it represents
      TimerView* view = new TimerView(fModel.GetTimer(i));
      // update the new height of this list component 
      newHeight += view->getHeight();
      
      // Add the timer view to our list and also as a child component. 
      this->addAndMakeVisible(fTimers.add(view));
}
   
   // update our size to force repainting. 
   // newHeight = jmax(300, newHeight);
   this->setSize(this->getWidth(), newHeight);
   
}
