/*
  ==============================================================================

    timerList.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"


class TimerModel;
class TimerView;

class TimerList   : public Component
                  , public ChangeListener   
{
public:
    TimerList(TimerModel& model);
    ~TimerList();

    void paint (Graphics&) override;
    void resized() override;
    
    /**
     * Called asynchronously when the timer model sends out a change broadcast. 
     * @param source broadcaster that has changed. 
     */
    void changeListenerCallback(ChangeBroadcaster* source) override;
    
    /**
     * When the number of items changes, rebuild our list of views. 
     */
    void Rebuild();
    
    

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimerList)
    
    TimerModel&  fModel;
    OwnedArray<TimerView> fTimers;
};
