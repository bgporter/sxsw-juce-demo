/*
  ==============================================================================

    timerView.cpp
    Author:  Brett Porter

  ==============================================================================
*/

#include "../../JuceLibraryCode/JuceHeader.h"
#include "timerView.h"
#include "../model/timer.h"


namespace 
{
   String FormatTime(int seconds) 
   {
      String formattedTime;
      // special case times > 1 hour
      if (seconds > (60*60))
      {
         auto hr = std::div(seconds, (60*60));
         formattedTime << String(hr.quot) << ":";
         seconds = hr.rem;
      }
      
      auto min = std::div(seconds, 60);
      formattedTime << String(min.quot).paddedLeft('0', 2) << ":";
      formattedTime << String(min.rem).paddedLeft('0', 2);
      
      
      return formattedTime;
   }
}


//==============================================================================
TimerView::TimerView(TimerItem* timer)
:  fTimer(timer)
{
   
   jassert(nullptr != timer);
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    
    // subscribe to changes in the timer so we can display them. 
    fTimer->addChangeListener(this);

    fTimerName.reset(new Label("name", timer->GetName()));
    this->addAndMakeVisible(fTimerName.get());
    fTimerName->setJustificationType(Justification::centredLeft);
    fTimerName->setFont(Font("Helvetica Neue", 18.f, Font::bold));
    fTimerName->setEditable(true, true, false);
    fTimerName->setColour(TextEditor::textColourId, Colours::white);
    fTimerName->setColour(TextEditor::backgroundColourId, Colour(0x00000000));
    fTimerName->addListener(this);
    
    fTimeRemaining.reset(new Label("time", FormatTime(fTimer->GetRemaining()) ));
    this->addAndMakeVisible(fTimeRemaining.get());
    fTimeRemaining->setFont(Font("Helvetica Neue", 18.f, Font::plain));
    fTimeRemaining->setJustificationType(Justification::centredRight);
    fTimeRemaining->setEditable(false, false, false);
    fTimeRemaining->setColour(TextEditor::textColourId, Colours::white);
    fTimeRemaining->setColour(TextEditor::backgroundColourId, Colour(0x00000000));
    
    
    
    
    


    // set an initial size..
    this->setSize(300, 75);

}

TimerView::~TimerView()
{
   // unsubscribe from the timer's updates.
   fTimer->removeChangeListener(this);
}


void TimerView::changeListenerCallback(ChangeBroadcaster* source)
{
   // ask to be repainted to update things. 
   fTimerName->setText(fTimer->GetName(), 
      NotificationType::dontSendNotification);
      
   fTimeRemaining->setText(FormatTime(fTimer->GetRemaining()), 
      NotificationType::dontSendNotification);
      
   this->repaint();
}

void TimerView::labelTextChanged(Label* label)
{
   // !!!
}

void TimerView::paint (Graphics& g)
{
   // create three colors: background, a moving bar for elapsed time, and 
   // a contrasting color for text. 
   Colour bgColor{fTimer->GetColor()};
   Colour elapsedColor{bgColor.withBrightness(0.5f)};
   Colour textColor{Colour::contrasting(bgColor, elapsedColor)};
   
   // clear the timer bar with the bg color. 
   if (! fTimer->IsFinished()) 
   {
      g.fillAll (bgColor);
      
      // make a new rectangle to show the elapsed time, growing from the right. 
      auto bounds = this->getLocalBounds().toFloat();
      float elapsedPercent = (1.f * fTimer->GetElapsed()) / fTimer->GetDuration();
      auto elapsedRect = bounds.removeFromRight(bounds.proportionOfWidth(elapsedPercent));
      g.setColour(elapsedColor);
      g.fillRect(elapsedRect);
   }
   else 
   {
      // if we're finished, just flash until we're cleared. 
      int seconds = static_cast<int>(fTimer->GetElapsed());
      g.fillAll((0 == seconds % 2) ? bgColor : elapsedColor);
   }

}

void TimerView::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..
    auto bounds = this->getLocalBounds();
    DBG("TimerView::resized() w = " << bounds.getWidth() << " h: " << bounds.getHeight());
    
    
    auto height = bounds.getHeight() / 3;
    auto timeWidth = bounds.getWidth() / 4;
    
    auto timeBox = bounds.removeFromRight(timeWidth);
    timeBox.removeFromRight(10);
    
    fTimerName->setBounds(bounds.getX(), height, bounds.getWidth(), height);
    fTimeRemaining->setBounds(timeBox.getX(), height, timeBox.getWidth(), height);
    

}
