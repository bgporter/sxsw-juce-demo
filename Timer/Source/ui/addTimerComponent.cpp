/*
  ==============================================================================

    addTimerComponent.cpp
    Author:  Brett Porter

  ==============================================================================
*/

#include "../../JuceLibraryCode/JuceHeader.h"
#include "addTimerComponent.h"

#include "../model/timerModel.h"

//==============================================================================
AddTimerComponent::AddTimerComponent(TimerModel* model)
:  fModel(model)
{
   fTimerName.reset(new TextEditor ("timerName"));
   addAndMakeVisible(fTimerName.get());
   fTimerName->setMultiLine(false);
   fTimerName->setReturnKeyStartsNewLine(false);
   fTimerName->setReadOnly(false);
   fTimerName->setScrollbarsShown(false);
   fTimerName->setCaretVisible(true);
   fTimerName->setPopupMenuEnabled(true);
   String defaultName{"Timer "};
   defaultName << fModel->NextTimerNumber();
   fTimerName->setText(defaultName);

   fTimerName->setBounds (16, 24, 300, 24);

   fNameLabel.reset (new Label ("timerName", TRANS("Timer name")));
   addAndMakeVisible(fNameLabel.get());
   fNameLabel->setFont(Font ("Helvetica Neue", 12.00f, Font::plain).withTypefaceStyle ("Regular"));
   fNameLabel->setJustificationType(Justification::centredLeft);
   fNameLabel->setEditable(false, false, false);
   fNameLabel->setColour(TextEditor::textColourId, Colours::black);
   fNameLabel->setColour(TextEditor::backgroundColourId, Colour (0x00000000));

   fNameLabel->setBounds (13, 53, 150, 14);

   fTimerDuration.reset (new TextEditor ("timer duration"));
   addAndMakeVisible (fTimerDuration.get());
   fTimerDuration->setMultiLine (false);
   fTimerDuration->setReturnKeyStartsNewLine (false);
   fTimerDuration->setReadOnly (false);
   fTimerDuration->setScrollbarsShown (true);
   fTimerDuration->setCaretVisible (true);
   fTimerDuration->setPopupMenuEnabled (true);
   fTimerDuration->setText("1:00");
   // limit input to 7 chars, just digits and colons
   fTimerDuration->setInputFilter(
      new TextEditor::LengthAndCharacterRestriction(7, String("0123456789:")),
      true);

   fTimerDuration->setBounds (16, 72, 71, 24);

   fDurationLabel.reset (new Label ("timerDuration", TRANS("Timer duration")));
   addAndMakeVisible(fDurationLabel.get());
   fDurationLabel->setFont (Font ("Helvetica Neue", 12.00f, Font::plain).withTypefaceStyle ("Regular"));
   fDurationLabel->setJustificationType(Justification::centredLeft);
   fDurationLabel->setEditable (false, false, false);
   fDurationLabel->setColour(TextEditor::textColourId, Colours::black);
   fDurationLabel->setColour(TextEditor::backgroundColourId, Colour (0x00000000));

   fDurationLabel->setBounds (13, 104, 150, 14);

   fOkButton.reset (new TextButton ("new button"));
   addAndMakeVisible (fOkButton.get());
   fOkButton->setButtonText (TRANS("OK"));
   fOkButton->addListener (this);

   fOkButton->setBounds (215, 140, 75, 24);

   fCancelButton.reset (new TextButton ("new button"));
   addAndMakeVisible (fCancelButton.get());
   fCancelButton->setButtonText (TRANS("Cancel"));
   fCancelButton->addListener (this);
   fCancelButton->setColour (TextButton::buttonColourId, Colour (0xffb0b0b0));

   fCancelButton->setBounds (60, 140, 75, 24);


   this->setSize(350, 200);


}

AddTimerComponent::~AddTimerComponent()
{
    fTimerName = nullptr;
    fNameLabel = nullptr;
    fTimerDuration = nullptr;
    fDurationLabel = nullptr;
    fOkButton = nullptr;
    fCancelButton = nullptr;
}

void AddTimerComponent::paint (Graphics& g)
{

    g.fillAll (Colours::lightgrey);   // clear the background

    g.setColour (Colours::grey);
    g.drawRect (getLocalBounds(), 1);   // draw an outline around the component

}

void AddTimerComponent::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}



void AddTimerComponent::buttonClicked(Button* b)
{
   if (fOkButton.get() == b)
   {
      String timerName = fTimerName->getText();
      String timerDur = fTimerDuration->getText();
      int duration = this->ParseDuration(timerDur);
      
      fModel->AddTimer(timerName, duration);
      
   }
   else if (fCancelButton.get() == b)
   {
      // no special handling of cancel, just exit. 
   }
   
   // look upwards in our parent chain to find an object of type "DialogWindow".
   // when you find it, tell it to exit its modal state. 
   // we don't care about the exit value passed back.
   DialogWindow* dw = this->findParentComponentOfClass<DialogWindow>();
   dw->exitModalState(0);
}




int AddTimerComponent::ParseDuration(const String& durationString)
{
   // split the duration apart by ":"
   StringArray tokens = StringArray::fromTokens(durationString, ":", String());
   
   // tokens will contain 0 or more strings. Right-most is seconds, we 
   // will attempt to handle minutes and hours. If there are more components 
   // in the duration string than that, we ignore.
   int hours = 0; 
   int minutes = 0; 
   int seconds = 0;
   
   int tokenCount = tokens.size();
   
   if (tokenCount >= 3)
   {
      hours = tokens[tokenCount-3].getIntValue();
   }
   if (tokenCount >= 2)
   {
      minutes = tokens[tokenCount-2].getIntValue();
   }
   if (tokenCount >= 1)
   {
      seconds = tokens[tokenCount-1].getIntValue();
   }
   
   return (hours * 60 * 60) + (minutes*60) + seconds;
   
}
