/*
  ==============================================================================

    alarm.cpp
    Created: 1 Mar 2019 11:13:04am
    Author:  Brett Porter

  ==============================================================================
*/

#include "alarm.h"


Alarm::Alarm()
:  fIsPlaying(false)
{
   fAudioManager.initialise(  0, // 0 input channels. 
                              2, // 2 output channels 
                              nullptr, // don't restore last state 
                              true    // selet default device on failure
                           );
   
   fFormatManager.registerBasicFormats();
   
   auto bellStream = new MemoryInputStream(BinaryData::Ship_Bell_wav,
      BinaryData::Ship_Bell_wavSize, false);
      
   jassert(nullptr != bellStream);
      
   auto reader = fFormatManager.createReaderFor(bellStream);
   jassert(nullptr != reader);
   
   // create the audio reader source and take ownership of it. 
   fReaderSource.reset(new AudioFormatReaderSource(reader, true));
   fReaderSource->setLooping(true);
   fTransportSource.setSource(fReaderSource.get());
   
   fPlayer.setSource(&fTransportSource);
   
   
   
   fAudioManager.addAudioCallback(&fPlayer);
}

Alarm::~Alarm()
{
   this->Stop();
   fAudioManager.removeAudioCallback(&fPlayer);
}

void Alarm::Start() 
{
   if (! fIsPlaying)
   {
      fTransportSource.setPosition(0.0);
      fTransportSource.setLooping(true);
      fTransportSource.start();
      fIsPlaying = true;
   }
   
}

void Alarm::Stop()
{
   if (fIsPlaying)
   {
      fTransportSource.stop();
      fIsPlaying = false;
   }
   
}

