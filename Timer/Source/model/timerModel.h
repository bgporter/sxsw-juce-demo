/*
  ==============================================================================

    timerModel.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"

#include "alarm.h"

class TimerItem;

class TimerModel  : public ChangeBroadcaster 
                  , public Timer
{
public:
   
   enum 
   {
      // request a timer callback every 500 ms. 
      kTimerInterval = 100
   };
   
   
   TimerModel(); 
   
   ~TimerModel();
   
   /**
    * Timer callback -- executed periodically when the current timer 
    * period elapses. We'll walk the list of timers and let each know that the 
    * time has changed. 
    */
   void timerCallback() override;
   
   /**
    * Create a new timer and add it to the end of our list. 
    * Will send a change notification out. 
    * @param name       Name to display. 
    * @param duration   timer duration in seconds. 
    */
   void AddTimer(StringRef name, int duration); 
   
   /**
    * Attempt to delete a timer from the list. Will send change update 
    * if successful. 
    * @param  index index of the timer to delete.  
    * @return       false if no such timer exists. 
    */
   bool DeleteTimer(int index);
   
   /**
    * Return a (non-owning) pointer to the requested timer by index. 
    * Don't try to delete this timer. 
    * @param  index timer index (0.. Size() -1)
    * @return       nullptr if the index is out of range
    */
   TimerItem* GetTimer(int index);
   
   /**
    * How many timers are in the model right now? 
    * @return number of timers. 
    */
   int Size() const; 
   
   /**
    * @return The sequence number of the next timer we'll create; can be used 
    *          for default names like "Timer 11"
    */
   int NextTimerNumber() const;
   
   /**
    * @return The number of timers that are complete and can be cleared. 
    */
   int GetCompleteTimerCount() const;
   
   /**
    * Delete any timers that are in a completed state. 
    * @return The number of timers that were deleted.
    */
   int ClearCompletedTimers();
   
   
   
   
private:
   OwnedArray<TimerItem> fTimers;
   
   /// total number of timers we've created; used to make default names.
   int fAddedCount;
   
   float fHue; 

   Alarm fAlarm;
   
   
};
