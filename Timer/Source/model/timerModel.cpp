/*
  ==============================================================================

    timerModel.cpp
    Author:  Brett Porter

  ==============================================================================
*/

#include "timerModel.h"
#include "timer.h"

TimerModel::TimerModel() 
:  fAddedCount{0}
,  fHue(0.f)
{
   this->startTimer(kTimerInterval);
}

TimerModel::~TimerModel()
{
   this->stopTimer();
}

void TimerModel::timerCallback() 
{
   uint32 now = Time::getMillisecondCounter();
   
   int timersCompleting = 0;
   for (auto t: fTimers)
   {
      if (t->Update(now))
      {
         ++timersCompleting;
      }
   }
   
   // notify listeners that something's changed. 
   if (timersCompleting > 0)
   {
      this->sendChangeMessage();
      fAlarm.Start();
   }
}

void TimerModel::AddTimer(StringRef name, int duration) 
{
   // convert the current hue value into a 32-but ARGB value to pass to the 
   // new timer object: 
   // args are hue, saturation, brightness, alpha, all between 0..1
   Colour hsb(fHue, 0.8f, 0.8f, 1.0f);
   auto newTimer = new TimerItem(name, hsb.getARGB(), duration);
   fTimers.add(newTimer);
   
   newTimer->Start(Time::getMillisecondCounter());

   // keep track of how many we've created.  
   ++fAddedCount;
   
   // update the hue for the next timer to add. Walk around the circle from 
   // 0.0 to 1.0, using modf to restrict the hue value to that range. 
   const float kHueDelta = 0.35f;
   float intPart; // we'll discard this, but the fn requires it. 
   fHue = modf(fHue + kHueDelta, &intPart);
   
   this->sendChangeMessage();
}

bool TimerModel::DeleteTimer(int index)
{
   // remove the object and delete it. 
   fTimers.remove(index, true);
   
   // notify observers! 
   this->sendChangeMessage();
   return true;
}

TimerItem* TimerModel::GetTimer(int index)
{
   // does range-checking internally; if index is out of range, 
   // will return a null pointer like we want. 
   return fTimers[index];
}


int TimerModel::Size() const
{
   return fTimers.size();
}


int TimerModel::NextTimerNumber() const
{
   return fAddedCount + 1;
}


int TimerModel::GetCompleteTimerCount() const
{
   int count = 0;
   
   for (auto t: fTimers)
   {
      if (t->IsFinished())
      {
         ++count;
      }
   }
   
   return count;
}

int TimerModel::ClearCompletedTimers()
{
   int cleared = 0;
   
   // delete in reverse order so that we don't double-delete!
   for (int i = fTimers.size()-1; i >= 0; --i)
   {
      auto t = this->GetTimer(i);
      if (t->IsFinished())
      {
         this->DeleteTimer(i);
         ++cleared;
      }
   }
      
   fAlarm.Stop()   ;
      
   return cleared;
}

