/*
  ==============================================================================

    timer.h
    Author:  Brett Porter

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"


class TimerItem : public ChangeBroadcaster 
{
public:
   TimerItem(StringRef name, uint32 color, int duration)
   :  fName(name)
   ,  fColor(color)
   ,  fDuration(duration)
   ,  fLastUpdated(0)
   ,  fMsElapsed(0)
   ,  fIsFinished(false)
   {
      
   };
   
   ~TimerItem()
   {
      
   };
   
   void SetName(const String& name) 
   {
      fName = name;
      this->sendChangeMessage();
   }
   
   String GetName() const { return fName; };
   
   uint32 GetColor() const { return fColor; };
   
   int GetDuration() const { return fDuration; };
   
   float GetElapsed() const { return fMsElapsed / 1000.f; };
   
   int GetRemaining() const 
   { 
      if (this->IsFinished())
      {
         return 0;
      }
      
      return fDuration - static_cast<int>(this->GetElapsed()); 
   };
   
   bool IsFinished() const { return fIsFinished; }; 
   
   /**
    * Start keeping track of time. 
    * @param msTime [description]
    */
   void Start(uint32 msTime)
   {
      fMsElapsed = 0;
      fLastUpdated = msTime;
      this->sendChangeMessage();
   };
   
   /**
    * Called on timer updates; may result in change notification being sent. 
    * @param msTime milliseconds since (probably) machine startup. 
    */
   bool Update(uint32 msTime)
   {
      
      fMsElapsed += (msTime - fLastUpdated);
      fLastUpdated = msTime;
      this->sendChangeMessage();
      
      // see if this update completes us. 
      if (! fIsFinished && (this->GetElapsed() >= fDuration))
      {
         fIsFinished = true;
         return true;
      }
      
      return false;
   }
   
private:
   /// name to display. 
   String fName;
   
   /// background color as argb
   uint32 fColor;
   
   /// total duration of this timer (in seconds). 
   int fDuration;
   
   /// millisecond count of the last time we were updated. 
   uint32 fLastUpdated;
   
   /// total # of ms we've seen go by since start. 
   uint32 fMsElapsed; 
   
   bool fIsFinished;
};
