/*
  ==============================================================================

    alarm.h
    Author:  Brett Porter


    Attribution: Using 'beep' sound from http://soundbible.com/1746-Ship-Bell.html
    Licensed under Creative Commons 

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"

class Alarm 
{
public:
   Alarm();
   
   ~Alarm();
   
   void Start(); 
   
   void Stop();
   
private:
   
   AudioDeviceManager fAudioManager;
   AudioFormatManager fFormatManager;
   AudioSourcePlayer  fPlayer;
   std::unique_ptr<AudioFormatReaderSource> fReaderSource;
   AudioTransportSource fTransportSource;
   
   
   bool fIsPlaying;
   
   
};
