/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

#include "ui/timerList.h"

MainComponent::MainComponent()
:  fControls(&fModel)
{
   this->addAndMakeVisible(fViewport);
   fViewport.setViewedComponent(new TimerList(fModel));
   fViewport.setScrollBarsShown(true, false);
   
   this->addAndMakeVisible(fControls);
   
   setSize (400, 600);
   
   
   // For now -- just create and start running a few timers 
   // until we can create them using the UI. 
   for (int i = 0; i < 5; ++i)
   {
      String name{"Timer "};
      name  << String(fModel.NextTimerNumber());
      
      fModel.AddTimer(name, 30 * (i+1));
      
   }
   
}

MainComponent::~MainComponent()
{
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    
    auto bounds = this->getLocalBounds();
    
    // remove a slice at the bottom for the control view. 
    auto controlBounds = bounds.removeFromBottom(75);
    
    fViewport.setBounds(bounds);
    fControls.setBounds(controlBounds);
}
