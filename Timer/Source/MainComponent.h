/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "model/timerModel.h"
#include "ui/controlView.h"
#include "ui/timerViewport.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...
    
    /// The timer model manages all of the timers and their logic. 
    TimerModel fModel;
    
    /// The viewport object owns the list of timer view objects and 
    /// implements all of the scrolling logic for us. 
    TimerViewport fViewport;
    
    ///  Bar at the bottom to hold the add/delete buttons.
    ControlView fControls;
    


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
