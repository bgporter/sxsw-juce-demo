/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   Ship_Bell_wav;
    const int            Ship_Bell_wavSize = 167464;

    extern const char*   outlineadd_circle_outline24px_svg;
    const int            outlineadd_circle_outline24px_svgSize = 298;

    extern const char*   outlineclear24px_svg;
    const int            outlineclear24px_svgSize = 249;

    extern const char*   Timer_png;
    const int            Timer_pngSize = 22427;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 4;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
