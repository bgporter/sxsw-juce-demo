# SXSW JUCE Demo

Simple demo application, created for SXSW 2019 Workshop "Native Cross-Platform Development With C++ & JUCE". 

## JUCE resources

This demo application is written to target JUCE 5.4.x, the current version in Spring of 2019. Useful links:

https://juce.com -- top level site with links to everything below. 

https://shop.juce.com/get-juce -- download the framework from ROLI's site, or you may prefer to clone the repository on github at https://github.com/WeAreROLI/JUCE.

https://docs.juce.com/master/index.html -- Documentation for the framework and all its classes. 

https://forum.juce.com/ -- very active community forum; also site of official announcements from the developers. 

https://juce.com/learn/tutorials -- continually growing collection of official tutorials. 

https://juce.com/discover/stories/coding-standards -- the programming style followed in the JUCE source. In the code for this application, you'll also see code written using the Art+Logic house style: http://styleguide.artandlogic.com/

Additionally, there are two directories inside the JUCE source distribution that will likely prove invaluable to you: 

`examples/` -- collection of applications that are more fully-functioned than the simpler tutorials that focus on individual aspects of JCUE development. 

`extras/` -- Additional standalone apps, including the source to the Projucer.

Time spent reading the source in these directories will repay itself many times over, both from a reference standpoint as well as gaining a better understanding of the capabilities of the framework and idioms to adopt that may be useful to you. 



